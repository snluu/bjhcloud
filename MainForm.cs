﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace BjhCloud
{
    public partial class MainForm : Form
    {
        const int BalloonTimeout = 3000;
        private Uploader uploader;
        private delegate Stream GetClipboardStreamDelegate();
        private delegate string GetExtensionFromClipboardDelegate();
        private delegate void SetClipboardTextDelegate(string text);

        public MainForm()
        {
            InitializeComponent();
            this.Visible = false;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            var homeDir = Environment.GetEnvironmentVariable("USERPROFILE");
            var filePath = Path.Combine(homeDir, "bjhcloud.json");

            if (!File.Exists(filePath))
            {
                var sampleConfig = new
                {
                    connectionString = "UseDevelopmentStorage=true",
                    containerName = "bhjcloud",
                    rootUrl = "http://127.0.0.1:10001/devstoreaccount1/bhjcloud/"
                };
                File.WriteAllText(filePath, JsonConvert.SerializeObject(sampleConfig, Formatting.Indented));
                MessageBox.Show(string.Format("{0} was created for you. Please enter the relevant information and restart the app!", filePath));
                Application.Exit();
            }

            dynamic config = JsonConvert.DeserializeObject(File.ReadAllText(filePath));
            this.uploader = new Uploader(Convert.ToString(config.connectionString), Convert.ToString(config.containerName), Convert.ToString(config.rootUrl));
        }

        private void mniExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void notifyIcon_Click(object sender, EventArgs e)
        {
            contextMenu.Show(Control.MousePosition);
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var uploadSource = (UploadSource)e.Argument;
            var worker = sender as BackgroundWorker;
            worker.ReportProgress(1);

            try
            {
                string result = null;
                switch (uploadSource)
                {
                    case UploadSource.Clipboard:
                        result = this.uploader.UploadStream(GetClipboardStream(), GetExtensionFromClipboard());
                        break;
                }

                worker.ReportProgress(100, result);
            }
            catch (Exception ex)
            {
                worker.ReportProgress(-1, ex.Message);
            }
            
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 1)
            {
                this.mniTextbox.Text = "Status: Uploading...";
                return;
            }
            
            if (e.ProgressPercentage == 100 && e.UserState != null)
            {
                var finalUrl = (string)e.UserState;
                this.mniTextbox.Text = finalUrl;
                SetClipboardText(finalUrl);

                this.notifyIcon.BalloonTipIcon = ToolTipIcon.Info;
                this.notifyIcon.BalloonTipText = string.Format("Upload succeeded! \"{0}\" is in your clipboard", finalUrl);
                this.notifyIcon.BalloonTipTitle = "Upload Succeeded!";
                this.notifyIcon.ShowBalloonTip(BalloonTimeout);
            }
            
            if (e.ProgressPercentage == -1 || e.UserState == null)
            {
                this.mniTextbox.Text = "Status: Ready";
                this.notifyIcon.BalloonTipIcon = ToolTipIcon.Error;
                this.notifyIcon.BalloonTipText = e.UserState == null ? "Unknown error" : (string)e.UserState;
                this.notifyIcon.BalloonTipTitle = "Upload failed!";
                this.notifyIcon.ShowBalloonTip(BalloonTimeout);
            }
        }

        private void mniUploadFromClipboard_Click(object sender, EventArgs e)
        {
            this.backgroundWorker.RunWorkerAsync(UploadSource.Clipboard);
        }

        private Stream GetClipboardStream()
        {
            if (InvokeRequired)
            {
                return (Stream)(Invoke(new GetClipboardStreamDelegate(GetClipboardStream)));
            }

            if (Clipboard.ContainsImage())
            {
                var stream = new MemoryStream();
                Clipboard.GetImage().Save(stream, ImageFormat.Png);
                return stream;
            }

            if (Clipboard.ContainsText())
            {
                var stream = new MemoryStream();                
                var buffer = Encoding.UTF8.GetBytes(Clipboard.GetText());
                stream.Write(buffer, 0, buffer.Length);
                return stream;
            }

            return null;
        }

        private string GetExtensionFromClipboard()
        {
            if (InvokeRequired)
            {
                return (string)(Invoke(new GetExtensionFromClipboardDelegate(GetExtensionFromClipboard)));
            }

            if (Clipboard.ContainsImage())
            {
                return "png";
            }

            if (Clipboard.ContainsText())
            {
                return "txt";
            }

            return null;
        }

        private void SetClipboardText(string text)
        {
            if (InvokeRequired)
            {
                Invoke(new SetClipboardTextDelegate(SetClipboardText));
            }

            Clipboard.SetText(text);
        }
    }
}
