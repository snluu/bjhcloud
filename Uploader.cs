﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;

namespace BjhCloud
{
    public class Uploader
    {
        private readonly Dictionary<string, string> mimeTypes = new Dictionary<string, string>
        {
            { "png", "image/png" },
            { "txt", "text/plain" },
        };

        private string ConnectionString { get; set; }
        private string ContainerName { get; set; }
        private string RootUrl { get; set; }

        public Uploader(string connectionString, string containerName, string rootUrl)
        {
            ConnectionString = connectionString;
            ContainerName = containerName;
            RootUrl = rootUrl;

            var container = GetContainer();
            container.CreateIfNotExists(BlobContainerPublicAccessType.Blob);
        }

        public string UploadFromFile(string filePath)
        {
            return null;
        }

        public string UploadStream(Stream stream, string extension)
        {
            stream.Seek(0, SeekOrigin.Begin);

            var blobName = string.Format("{0}.{1}", Guid.NewGuid(), extension);
            var container = GetContainer();
            var blob = container.GetBlockBlobReference(blobName);

            extension = extension.ToLowerInvariant();
            if (this.mimeTypes.ContainsKey(extension))
            {
                blob.Properties.ContentType = this.mimeTypes[extension];
            }
            else {
                blob.Properties.ContentType = "application/octet-stream";
            }

            blob.UploadFromStream(stream);

            string url;
            if (RootUrl.EndsWith("/"))
            {
                url = RootUrl + blobName;
            }
            else
            {
                url = string.Format("{0}/{1}", RootUrl, blobName);
            }

            return ShortenUrl(url);
        }

        private string ShortenUrl(string url)
        {
            var shawtyGetUrl = string.Format(
                "http://luu.bz/shawty.js?url={0}",
                HttpUtility.UrlEncode(url)
            );

            string content = null;

            var request = (HttpWebRequest)WebRequest.Create(shawtyGetUrl);
            request.Method = "GET";

            var response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                using (var contentStream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(contentStream))
                    {
                        content = reader.ReadToEnd();
                    }
                }
            }

            if (content == null || !content.StartsWith("shawty(") || !content.EndsWith(");"))
            {
                return null;
            }

            content = content.Substring(7, content.Length - 9);
            var json = JsonConvert.DeserializeObject<Dictionary<string, string>>(content);

            if (!json.ContainsKey("success") || !json.ContainsKey("short"))
            {
                return null;
            }

            int success = 0;
            int.TryParse(json["success"], out success);

            if (success != 1)
            {
                return null;
            }

            return json["short"];
        }

        private CloudBlobContainer GetContainer()
        {
            var account = CloudStorageAccount.Parse(ConnectionString);
            var client = account.CreateCloudBlobClient();
            return client.GetContainerReference(ContainerName);
        }
    }
}
